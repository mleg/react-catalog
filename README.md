This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

Below you will find some information on how to perform common tasks.<br>
You can find the most recent version of this guide [here](https://github.com/facebookincubator/create-react-app/blob/master/packages/react-scripts/template/README.md).

## Примечания к решению

-  **Redux** -- это очень много boilerplate-кода, использовал [MobX](https://github.com/mobxjs/mobx) для state-менеджмента. В реальном проекте, если React, использовал бы основанный на нём [mobx-state-tree](https://github.com/mobxjs/mobx-state-tree). Для тестовой задачи `mobx-state-tree` больно тяжёлая артиллерия.
-  Всё-таки использовал **Typescript**, а не голый JS, уж больно на нём приятней писать на React-е и MobX.
-  Конечно, никакой адаптации к мобильному виду не производил, это за пределами учебной задачи.
