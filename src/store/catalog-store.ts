import { computed, observable } from 'mobx'
import {
	convertToCurrency,
	Currency,
	formatSum,
	RUB,
} from '../common/currencies'
import { generateId } from '../common/utils'

export interface CatalogItem {
	key: string
	name: string
	quantity: number
	price: number
}

export interface CatalogItemView extends CatalogItem {
	priceF: string
	sum: number
	sumF: string
}

const rawItmes: CatalogItem[] = require('./catalog.json')

export class CatalogStore {
	@observable items: CatalogItem[] = addKey(rawItmes)

	@observable currency: Currency = RUB

	@observable
	editing = {
		key: '',
		col: '',
	}

	@computed
	get itemsView(): CatalogItemView[] {
		return this.items.map(item => {
			const price = convertToCurrency(item.price, this.currency)
			const sum = item.quantity * price
			return {
				...item,
				price,
				priceF: formatSum(price),
				sum,
				sumF: formatSum(sum),
			}
		})
	}

	@computed
	get total(): string {
		const totalRub = this.items.reduce(
			(res, item) => res + item.price * item.quantity,
			0,
		)
		const total = convertToCurrency(totalRub, this.currency)
		return formatSum(total, this.currency)
	}

	setCurrency(currency) {
		this.currency = currency
	}
}

function addKey(items: CatalogItem[]): CatalogItem[] {
	return items.map(item => ({
		...item,
		key: generateId(),
	}))
}
