export type Currency = 'RUB' | 'USD'

const EXCHANE_RATE = 60
export const RUB: Currency = 'RUB'
export const USD: Currency = 'USD'

export function convertToCurrency(
	numberRub: number,
	intoCurrency: Currency,
): number {
	return intoCurrency === RUB ? numberRub : numberRub / EXCHANE_RATE
}

export function formatSum(sum: number, currency?: Currency): string {
	const options = currency
		? {
				style: 'currency',
				currency,
		  }
		: {
				style: 'decimal',
				minimumFractionDigits: 2,
				maximumFractionDigits: 2,
		  }
	return new Intl.NumberFormat('ru-RU', options).format(sum)
}
