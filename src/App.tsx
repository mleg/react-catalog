import './App.css'
import { observer, inject } from 'mobx-react'
import * as React from 'react'
import { CatalogStore } from './store/catalog-store'
import { Total } from './components/Total'
import { CatalogTable } from './components/CatalogTable'
import { CurrencyToggle } from './components/CurrencyToggle'

interface Props {
	catalog?: CatalogStore
}
@inject('catalog')
@observer
export class App extends React.Component<Props> {
	public render() {
		return (
			<div className="App">
				<div className="App__item">
					<CurrencyToggle />
				</div>
				<div className="App__item">
					<CatalogTable />
				</div>
				<div className="App__item">
					<Total />
				</div>
			</div>
		)
	}
}
