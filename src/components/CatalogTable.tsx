import { observer, inject } from 'mobx-react'
import * as React from 'react'
import { Table } from 'antd'
import { ColumnProps } from 'antd/lib/table'
import { CatalogStore } from '../store/catalog-store'

interface Props {
	catalog?: CatalogStore
}

interface ColumnPropsExtended<T> extends ColumnProps<T> {
	getTitle?(props: any): string
}

const sorter = feild => (a, b) => (a[feild] >= b[feild] ? 1 : -1)

const columnSettings: Array<ColumnPropsExtended<{}>> = [
	{
		title: 'Товар',
		dataIndex: 'name',
		key: 'name',
		width: '45%',
		sorter: sorter('name'),
	},
	{
		title: 'Количество',
		dataIndex: 'quantity',
		key: 'quantity',
		align: 'right',
		width: '15%',
		sorter: sorter('quantity'),
	},
	{
		getTitle({ currency }) {
			return `Cтоимость за единицу, ${currency}`
		},
		dataIndex: 'priceF',
		key: 'priceF',
		align: 'right',
		width: '20%',
		sorter: sorter('price'),
	},
	{
		title: 'Сумма',
		dataIndex: 'sumF',
		key: 'sumF',
		align: 'right',
		width: '20%',
		sorter: sorter('sum'),
	},
]

@inject('catalog')
@observer
export class CatalogTable extends React.Component<Props> {
	render() {
		const { itemsView, currency } = this.props.catalog!

		const columns = columnSettings.map(col => {
			const columnProps = { currency }
			return {
				...col,
				title: col.getTitle ? col.getTitle(columnProps) : col.title,
			}
		}) as Array<ColumnProps<{}>>

		return (
			<Table
				columns={columns}
				dataSource={itemsView}
				pagination={false}
				bordered={true}
			/>
		)
	}
}
