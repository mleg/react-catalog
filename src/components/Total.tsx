import './Total.css'
import * as React from 'react'
import { observer, inject } from 'mobx-react'
import { CatalogStore } from '../store/catalog-store'

interface Props {
	catalog?: CatalogStore
}

@inject('catalog')
@observer
export class Total extends React.Component<Props> {
	render() {
		const { catalog } = this.props
		return (
			<div className="Total">
				Итого: <span className="Total__sum">{catalog!.total}</span>
			</div>
		)
	}
}
