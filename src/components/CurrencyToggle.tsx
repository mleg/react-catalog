import './CurrencyToggle.css'
import * as React from 'react'
import { observer, inject } from 'mobx-react'
import { CatalogStore } from '../store/catalog-store'
import { Switch } from 'antd'
import { RUB, USD, Currency } from '../common/currencies'

interface Props {
	catalog?: CatalogStore
}

@inject('catalog')
@observer
export class CurrencyToggle extends React.Component<Props> {
	render() {
		const onChange = checked => {
			this.props.catalog!.setCurrency(checked ? RUB : USD)
		}
		const { currency } = this.props.catalog!
		const defaultChecked = currency === RUB

		const currencyClasses = (styledCurrency: Currency) =>
			[
				'CurrencyToggle__item',
				'CurrencyToggle__currency',
				styledCurrency === currency
					? 'CurrencyToggle__currency--active'
					: null,
			]
				.filter(Boolean)
				.join(' ')

		return (
			<div className="CurrencyToggle">
				<span className={currencyClasses('USD')}>USD</span>
				<div className="CurrencyToggle__item">
					<Switch defaultChecked={defaultChecked} onChange={onChange} />
				</div>
				<span className={currencyClasses('RUB')}>RUB</span>
			</div>
		)
	}
}
