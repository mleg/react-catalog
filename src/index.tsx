import 'antd/dist/antd.css'
import * as React from 'react'
import * as ReactDOM from 'react-dom'
import { App } from './App'
import { Provider } from 'mobx-react'
import registerServiceWorker from './registerServiceWorker'
import { CatalogStore } from './store/catalog-store'

const catalog = new CatalogStore()

ReactDOM.render(
	<Provider catalog={catalog}>
		<App />
	</Provider>,
	document.getElementById('root') as HTMLElement,
)
registerServiceWorker()
